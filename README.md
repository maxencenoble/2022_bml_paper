# From Deep Neural Network to Gaussian Process (dnn2gp)

This repository contains code to reproduce results for the paper *Approximate Inference Turns Deep Networks into Gaussian Processes*

Original code: https://github.com/team-approx-bayes/dnn2gp

### 1. Computing and Visualizing Kernels and Predictive Distribution

The results are readily available in directory `/results`.

We use pre-trained LeNet-5 models that are saved in the `/models` directory and are trained on 
either CIFAR-10, MNIST, FASHION-MNIST. We trained our own model on FASHION-MNIST (see notebook `FashionMNIST_training.ipynb`).

To reproduce experiments :
```bash
python kernel_and_predictive.py --name mnist
python kernel_and_predictive.py --name cifar
python kernel_and_predictive.py --name fashion_mnist # delta=10^-4
python kernel_and_predictive.py --name fashion_mnist2 # delta=10^-2
python kernel_and_predictive.py --name fashion_mnist3 # delta=10^-6
```

To reproduce the plots of kernel, predictive mean, and variance :
```bash
python kernel_and_predictive_plots.py --name mnist
python kernel_and_predictive_plots.py --name cifar
python kernel_and_predictive_plots.py --name fashion_mnist # delta=10^-4
python kernel_and_predictive_plots.py --name fashion_mnist2 # delta=10^-2
python kernel_and_predictive_plots.py --name fashion_mnist3 # delta=10^-6
```


### 2. Computing Hyperparameter Tuning Experiments

The results are readily available in directory `/results`.

To reproduce experiments on synthetic data:
```bash
python marglik.py --name original --experiment delta # regularization parameter
python marglik.py --name original --experiment width # width parameter
python marglik.py --name new --experiment delta # regularization parameter
python marglik.py --name new --experiment width # width parameter
```

Measurements will be saved in in directory `/results`.

To plots the results (synthetic data):
```bash
python marglik_plots.py --name original --mode delta
python marglik_plots.py --name original --mode width
python marglik_plots.py --name new --mode delta
python marglik_plots.py --name new --mode width
```

To plots the results (real-world data):
```bash
python marglik_plots.py --name wine
```


### Requirements

```bash
pip install -r requirements.txt
```
